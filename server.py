"""Python Flask API Auth0 integration example
"""

from os import environ as env
from dotenv import load_dotenv, find_dotenv
from flask import Flask, jsonify
from authlib.integrations.flask_oauth2 import ResourceProtector
from validator import Auth0JWTBearerTokenValidator

require_auth = ResourceProtector()
validator = Auth0JWTBearerTokenValidator(
    "dev-squcdk8nm8k1rz66.us.auth0.com",
    "http://rest-api.103.52.115.226.nip.io:5000/"
)
require_auth.register_token_validator(validator)

APP = Flask(__name__)


@APP.route("/")
def index():
    welcome_msg = """
                <H1> Flask OAuth2 Demo using auth0<br>
                <a href="/api/public">/api/public</a><br>
                <a href="/api/private">/api/private</a><br>
                <a href="/api/private-scoped">/api/private-scoped</a>
                """
    return welcome_msg


@APP.route("/api/public")
def public():
    """No access token required."""
    response = (
        "Hello from a public endpoint! You don't need to be"
        " authenticated to see this."
    )
    return jsonify(message=response)


@APP.route("/api/private")
@require_auth(None)
def private():
    """A valid access token is required."""
    response = (
        "Hello from a private endpoint! You need to be"
        " authenticated to see this."
    )
    return jsonify(message=response)


@APP.route("/api/private-read")
@require_auth("read:messages")
def private_read_scope():
    """A valid access token and scope are required."""
    response = "Anda memiliki read:messages permission"
    return jsonify(message=response)


@APP.route("/api/private-create")
@require_auth("create:messages")
def private_create_scope():
    """A valid access token and scope are required."""
    response = "Anda memiliki create:messages permission"
    return jsonify(message=response)


if __name__ == "__main__":
    APP.run(debug=True)